package tdlm.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Shape;
import tdlm.gui.Workspace;
import saf.components.AppDataComponent;
import saf.AppTemplate;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // FIRST THE THINGS THAT HAVE TO BE SAVED TO FILES
    
    // NAME OF THE TODO LIST
    StringProperty name;
    
    // LIST OWNER
    StringProperty owner;
    
    // THESE ARE THE ITEMS IN THE TODO LIST
    ObservableList<ToDoItem> items;
    
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
        
        owner = new SimpleStringProperty();
        
        name = new SimpleStringProperty();
        
	app = initApp;

        items = FXCollections.observableArrayList();
        
    }
    
    public ObservableList<ToDoItem> getItems() {
	return items;
    }
    
    public String getName() {
        
        return name.get();
    }
    
    public String getOwner() {
        
        return owner.get();
    }

    public void addItem(ToDoItem item) {
  

        items.add(item);
    }

    public AppTemplate getApp(){
        
        return app;
    }
    
    public void remove(){
        
        items.remove(0);
    }
    
    public void updateItem(ToDoItem item){
        
        
    }

    /**
     * 
     */
    @Override
    public void reset() {
        
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        this.addName("");
        this.addOwner("");
        
        items.clear();
        
    }
    
    public void addName(String name){
        
        this.name.setValue(name);  
    }
    
    public void addOwner(String owner){
        
        this.owner.setValue(owner);
    }
}
