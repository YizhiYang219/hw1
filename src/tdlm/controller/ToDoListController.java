package tdlm.controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import static java.time.temporal.TemporalQueries.localDate;
import java.util.Date;
import javafx.beans.property.BooleanProperty;
import static javafx.beans.property.BooleanProperty.booleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import properties_manager.PropertiesManager;
import tdlm.data.DataManager;
import tdlm.gui.Workspace;
import saf.AppTemplate;
import static saf.settings.AppPropertyType.APP_CSS;
import static saf.settings.AppPropertyType.APP_PATH_CSS;
import static saf.settings.AppPropertyType.SAVE_ICON;
import static saf.settings.AppPropertyType.SAVE_TOOLTIP;
import tdlm.PropertyType;
import tdlm.data.ToDoItem;
import static saf.settings.AppStartupConstants.CLOSE_BUTTON_LABEL;
import saf.components.AppStyleArbiter;

/**
 * This class responds to interactions with todo list editing controls.
 *
 * @author McKillaGorilla
 * @version 1.0
 */
public class ToDoListController implements AppStyleArbiter{

    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_BACKGROUND_COLOR = "background_color";
    public static final String YES = "Yes";
    public static final String OK = "OK";
    public static final String NO = "No";
    public static final String CANCEL = "Cancel";

    AppTemplate app;

    public ToDoListController(AppTemplate initApp) {
        app = initApp;
    }

    public void processAddItem() {
        // ENABLE/DISABLE THE PROPER BUTTONS
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace();

        PropertiesManager props = PropertiesManager.getPropertiesManager();

        DataManager data = (DataManager) app.getDataComponent();

        GridPane promptForNewToDoItem = new GridPane();

        
        
        promptForNewToDoItem.setAlignment(Pos.CENTER);

        promptForNewToDoItem.setHgap(5.5);
        promptForNewToDoItem.setVgap(5.5);

        TextField category = new TextField();
        
        Label label1 = new Label(props.getProperty(PropertyType.CATEGORY_COLUMN_HEADING));       
        promptForNewToDoItem.add(label1, 0, 0);
        promptForNewToDoItem.add(category, 1, 0);
        
        TextField description = new TextField();
        Label label2 = new Label(props.getProperty(PropertyType.DESCRIPTION_COLUMN_HEADING));
        
        
        promptForNewToDoItem.add(label2, 0, 1);
        promptForNewToDoItem.add(description, 1, 1);

        LocalDate currentDate = LocalDate.now();
        DatePicker startDate = new DatePicker(currentDate);

        promptForNewToDoItem.add(new Label(props.getProperty(PropertyType.START_DATE_COLUMN_HEADING)), 0, 2);
        promptForNewToDoItem.add(startDate, 1, 2);

        DatePicker endDate = new DatePicker(currentDate);
        promptForNewToDoItem.add(new Label(props.getProperty(PropertyType.END_DATE_COLUMN_HEADING)), 0, 3);
        promptForNewToDoItem.add(endDate, 1, 3);

        CheckBox checkBox = new CheckBox();
        promptForNewToDoItem.add(new Label(props.getProperty(PropertyType.COMPLETED_COLUMN_HEADING)), 0, 4);
        promptForNewToDoItem.add(checkBox, 1, 4);


        Button ok = new Button(props.getProperty(PropertyType.OK));
        Button cancel = new Button(props.getProperty(PropertyType.CANCEL));

        
        
        promptForNewToDoItem.add(ok, 0, 7);
        promptForNewToDoItem.add(cancel, 1, 7);

        Stage stage = new Stage();
       
        Scene scene = new Scene(promptForNewToDoItem, 500, 500);

        stage.setTitle(props.getProperty(PropertyType.WORKSPACE_HEADING_LABEL));
        
        scene.getStylesheets().add("tdlm/css/tdlm_style.css");
        promptForNewToDoItem.getStyleClass().add("cust");
        
        stage.setScene(scene);

        stage.show();

        ok.setOnAction(e -> {
            ToDoItem item = createToDoItem(category.getText(), description.getText(), startDate.getValue(), endDate.getValue(), checkBox.isSelected());
            data.addItem(item);
            workspace.upDateWorkspace();
            workspace.getTableView().getSelectionModel().clearSelection();

            // need to be fix 
            app.getGUI().updateToolbarControls(false);
            stage.close();
        });

        cancel.setOnAction(e -> stage.close());
    }

    public void processRemoveItem() {

        Workspace workspace = (Workspace) app.getWorkspaceComponent();

        DataManager data = (DataManager) app.getDataComponent();

        TableView<ToDoItem> table = workspace.getTableView();

        ToDoItem item = table.getSelectionModel().getSelectedItem();

        data.getItems().remove(item);

        if (table.getItems().size() == 0) {
            workspace.getRemoveItemButton().setDisable(true);
            workspace.getMoveUpItemButton().setDisable(true);
            workspace.getMoveDownItemButton().setDisable(true);
        }

        if (table.getItems().size() != 0) {

            app.getGUI().updateToolbarControls(false);
        }
    }

    public void processMoveUpItem() {

        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        DataManager data = (DataManager) app.getDataComponent();

        TableView<ToDoItem> table = workspace.getTableView();

        ToDoItem source = table.getSelectionModel().getSelectedItem();

        int sourceIndex = table.getItems().indexOf(source);

        ToDoItem target;
        if (sourceIndex == 0) {
            workspace.getMoveUpItemButton().setDisable(true);
        } else {
            target = table.getItems().get(sourceIndex - 1);
            table.getItems().remove(sourceIndex - 1);
            table.getItems().add(sourceIndex - 1, source);
            table.getItems().remove(sourceIndex);
            table.getItems().add(sourceIndex, target);
        }

    }

    public void processMoveDownItem() {

        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        DataManager data = (DataManager) app.getDataComponent();

        TableView<ToDoItem> table = workspace.getTableView();

        // this is part is important
        ToDoItem source = table.getSelectionModel().getSelectedItem();

        int sourceIndex = table.getItems().indexOf(source);

        if (sourceIndex == table.getItems().size() - 1) {
            workspace.getMoveDownItemButton().setDisable(true);
        } else {
            ToDoItem target = table.getItems().get(sourceIndex + 1);
            table.getItems().remove(sourceIndex + 1);

            table.getItems().add(sourceIndex, target);
        }
    }

    public void processEditItem() {

        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        DataManager data = (DataManager) app.getDataComponent();

        TableView<ToDoItem> table = workspace.getTableView();

        ToDoItem item = table.getSelectionModel().getSelectedItem();

        PropertiesManager props = PropertiesManager.getPropertiesManager();

        GridPane promptForEditToDoItem = new GridPane();
        promptForEditToDoItem.setAlignment(Pos.CENTER);

        promptForEditToDoItem.setHgap(5.5);
        promptForEditToDoItem.setVgap(5.5);

        TextField category = new TextField(item.getCategory());
        promptForEditToDoItem.add(new Label(props.getProperty(PropertyType.CATEGORY_COLUMN_HEADING)), 0, 0);
        promptForEditToDoItem.add(category, 1, 0);

        TextField description = new TextField(item.getDescription());
        promptForEditToDoItem.add(new Label(props.getProperty(PropertyType.DESCRIPTION_COLUMN_HEADING)), 0, 1);
        promptForEditToDoItem.add(description, 1, 1);

        DatePicker startDate = new DatePicker(item.getStartDate());
        promptForEditToDoItem.add(new Label(props.getProperty(PropertyType.START_DATE_COLUMN_HEADING)), 0, 2);
        promptForEditToDoItem.add(startDate, 1, 2);
        //startDate.toString();

        DatePicker endDate = new DatePicker(item.getEndDate());
        promptForEditToDoItem.add(new Label(props.getProperty(PropertyType.END_DATE_COLUMN_HEADING)), 0, 3);
        promptForEditToDoItem.add(endDate, 1, 3);

        CheckBox checkBox = new CheckBox();
        checkBox.setSelected(item.getCompleted());
        promptForEditToDoItem.add(new Label(props.getProperty(PropertyType.COMPLETED_COLUMN_HEADING)), 0, 4);
        promptForEditToDoItem.add(checkBox, 1, 4);

        Button ok = new Button(props.getProperty(PropertyType.OK));
        Button cancel = new Button(props.getProperty(PropertyType.CANCEL));

        promptForEditToDoItem.add(ok, 0, 7);
        promptForEditToDoItem.add(cancel, 1, 7);

        Stage stage = new Stage();

        Scene scene = new Scene(promptForEditToDoItem, 500, 500);

        stage.setTitle(props.getProperty(PropertyType.WORKSPACE_HEADING_LABEL));

        scene.getStylesheets().add("tdlm/css/tdlm_style.css");
        promptForEditToDoItem.getStyleClass().add("cust");
        
        stage.setScene(scene);

        stage.show();

        ok.setOnAction(e -> {
            ToDoItem innerItem = createToDoItem(category.getText(), description.getText(), startDate.getValue(), endDate.getValue(), checkBox.isSelected());

            data.getItems().indexOf(item);
            data.getItems().remove(item);
            data.addItem(innerItem);
            table.getSelectionModel().select(data.getItems().size() - 1);
            table.getSelectionModel().focus(data.getItems().size() - 1);
            
            int index = table.getSelectionModel().getSelectedIndex();
            
            if(index == 0){
                workspace.getMoveDownItemButton().setDisable(false);
                workspace.getMoveUpItemButton().setDisable(true);
            }
            else if(index == data.getItems().size() - 1){
                workspace.getMoveUpItemButton().setDisable(false);
                workspace.getMoveDownItemButton().setDisable(true);
            }
            
            stage.close();
        });

        cancel.setOnAction(e -> stage.close());
    }

    private ToDoItem createToDoItem(String category, String description, LocalDate startDate, LocalDate endDate, boolean completed) {

        ToDoItem item = new ToDoItem();
        item.setCategory(category);
        item.setDescription(description);
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        item.setCompleted(completed);

        return item;
    }
    
    public void initStyle() {

    }
}
